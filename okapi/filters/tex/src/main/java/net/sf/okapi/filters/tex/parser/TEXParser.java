/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.tex.parser;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;

//import net.sf.okapi.filters.markdown.Parameters;

public class TEXParser {
    //private static final Parser PARSER = Parser.builder(OPTIONS).build();

    private String newline = System.lineSeparator();
    //private Node root = null;
    private Deque<TEXToken> tokenQueue = new LinkedList<>();
    private boolean lastAddedTranslatableContent = false;
    private int numNonTranslatableNewlines = 0;
//    private Parameters params;

    /**
     * Create a new {@link TEXParser} that uses the platform-specific newline.
     */
	public TEXParser() {
		//this.params = params;
	}

    /**
     * Create a new {@link TEXParser} that uses the specified string as a newline.
     * @param newline The newline type that this parser will use
     */
    public TEXParser(String newline) {
        this.newline = newline;
    }
    
    /**
     * Return category number to which given string corresponds
			http://www.ctex.org/documents/shredder/src/texbook.pdf
     * @param c
     * @return int
     */
    private int findStringCategory(String c) {
        if (c.equals("\\")) { 			// Escape character (control sequence follows)
            return 0;
        } else if (c.equals("{")) {		// beginning of group
            return 1;
        } else if (c.equals("}")) {		// end of group
            return 2;
        } else if (c.equals("$")) {		// math shift
            return 3;
        } else if (c.equals("&")) {		// Alignment tab (table column separator)
            return 4;
        } else if (c.matches("[\n]")) {	// end of line [\r\n] ; \r is ignored and dropped
            return 5;
        } else if (c.equals("#")) {		// parameter
            return 6;
        } else if (c.equals("^")) {		// superscript
            return 7;
        } else if (c.equals("_")) {		// subscript
            return 8;
        } else if (c.matches("[\r]")) {	// ignored character	\r is ignored and dropped
            return 9;
        } else if (c.matches("[ \t]")) {//space
            return 10;
        } else if (c.matches("[a-zA-Z0-9]")) {
            return 11;
        } else if (c.equals("~")) {		// active character
            return 13;
        } else if (c.equals("%")) {		// comment character
            return 14;					
        } else if (c.equals("\0000")) {	// "Invalid character" 
        	return 15;					
        }
        return 12;
    }
    
    /**
     * Parse the given TEX content into tokens that can be then retrieved with
     * calls to {@link TEXParser#getNextToken()}. Any existing tokens from
     * previous calls to {@link TEXParser#parse(String)} will be discarded.
     *
     * @param texContent The TEX content to parse into tokens
	 * Inspired from:
     * https://github.com/omegat-org/omegat/blob/master/src/org/omegat/filters2/latex/LatexFilter.java
     */
    public void parse(String texContent) {
        tokenQueue.clear();
        numNonTranslatableNewlines = 0;
        lastAddedTranslatableContent = false;

        StringBuilder par = new StringBuilder();
        StringBuilder comment = new StringBuilder();
        LinkedList<String> commands = new LinkedList<String>();
        
        String[] c; //Split texContent into characters
        if (texContent.isEmpty()) {
        	c = new String[0];
        } else {
        	c = texContent.split("(?!^)");
        }
        
        int idx = 0;
        while (idx < c.length) {

        	String cidx = c[idx];
        	int cat = findStringCategory(cidx);
        	
        	if (cat == 0) { // "/"
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		/* parse control sequence */
        		/* https://en.wikibooks.org/wiki/LaTeX/Basics */
        		StringBuilder cmd = new StringBuilder();
        		cmd.append(cidx);
        		idx++;
        		while (idx < c.length) {
        			// cmd tagad satur "/" komandā var būt tikai a-Z0-9 un arī []
        			String cmdc = c[idx]; 
        			
        			if (findStringCategory(cmdc) == 11) {
        				cmd.append(cmdc);//ordinary char "[a-zA-Z0-9]"
        			} else if (findStringCategory(cmdc) == 12) {
        				cmd.append(cmdc);// [] and all others 
        				// if cmd contains [ then everythin till ] is also command
        				if ( cmdc.equals("[") ) {
        					// last char was [, append everything till ] found
        					idx++;
            				while (idx < c.length) {
            					cmdc = c[idx];
                            	if ( findStringCategory(cmdc) == 9 ) { 
                            		idx++;
                            		continue;
                            	}
            					if ( cmdc.equals("]") ) {
            						cmd.append(cmdc);
            						break;
            					}
            					cmd.append(cmdc);
            					idx++;
            				}       					
        				}
        			} else if ( findStringCategory(cmdc) == 9 ) { 
        				// ignored char
        				idx++;
                		continue;
                	} else if (cmd.length() == 1) { // "/" un kāds specsimbols
        				cmd.append(cmdc);
        				break;
                	} else if ( findStringCategory(cmdc) == 10) { // command is done, space gets added to end of command
        				cmd.append(cmdc);
        				break;
        			} else { // command is done
//        				System.out.println(idx);
        				idx--;
        				break;
        			}
        			idx++;
        		}
        		tokenQueue.addLast(new TEXToken(cmd.toString(), false, TEXTokenType.COMMAND));//@TODO - check this
        	} else if (cat == 1) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("{", false, TEXTokenType.OPEN_CURLY));
        	} else if (cat == 2) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("}", false, TEXTokenType.CLOSE_CURLY));
        	} else if (cat == 3) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("$", false, TEXTokenType.DOLLAR));
        	} else if (cat == 4) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("&", false, TEXTokenType.AMPERSAND));
        	} else if (cat == 5) { // \r\n or  [\r\n]
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("\n", false, TEXTokenType.NEWLINE));
        	} else if (cat == 6) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("#", false, TEXTokenType.HASHTAG));
        	} else if (cat == 7) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("^", false, TEXTokenType.CARET));
        	} else if (cat == 8) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("_", false, TEXTokenType.UNDERSCORE));
        	} else if (cat == 9) {
        		// ignore IGNORED_CHAR
//        		if (par.length()>0) {
//        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
//        			par.setLength(0);
//        		}
//        		tokenQueue.addLast(new TEXToken("\000", false, TEXTokenType.IGNORED_CHAR));
        	} else if (cat == 10) { //"[ \t]"
                par.append(cidx);
        	} else if (cat == 11) { //"~"
                par.append(cidx);
        	} else if (cat == 13) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("~", false, TEXTokenType.TILDE));
        	} else if (cat == 14) { // "%"
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
                /* build comment */
        		comment.setLength(0);
                comment.append(cidx);
                idx++;

                while (idx < c.length) {
                	if ( findStringCategory(c[idx]) == 5 ) { 
                		break;
                	}
                	if ( findStringCategory(c[idx]) == 9 ) { 
                		idx++;
                		continue;
                	}
                    String commentc = c[idx];
                    comment.append(commentc);
                    idx++;
                }
                tokenQueue.addLast(new TEXToken(comment.toString(), false, TEXTokenType.COMMENT));
                if ( idx <c.length ) {
                	tokenQueue.addLast(new TEXToken(c[idx], false, TEXTokenType.NEWLINE));
                }
        	} else if (cat == 15) {
        		if (par.length()>0) {
        			tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        			par.setLength(0);
        		}
        		tokenQueue.addLast(new TEXToken("\0000", false, TEXTokenType.INVALID_CHAR));
        	} else { //non-control char(,:- etc.) - process as ordinary char == no spaces here
                par.append(cidx);
        	}
        	idx++;
        }
        // done looping, save token, if last token was text
        if ( par.length() > 0) {
        	tokenQueue.addLast(new TEXToken(par.toString(), true, TEXTokenType.TEXT));
        }
    }

    public boolean hasNextToken() {
        return !tokenQueue.isEmpty();
    }

    /**
     * Returns the next available token and removes it from parser
     *
     * @return The next token
     * @throws IllegalStateException If no more tokens are remaining
     */
    public TEXToken getNextToken() {
        if (!hasNextToken()) {
            throw new IllegalStateException("No more tokens remaining");
        }
        return tokenQueue.removeFirst();
    }
    
    /**
     * Returns the next available token and keeps it in parser 
     * @return The next token null If no more tokens are remaining
     */
    public TEXToken peekNextToken() {
        return tokenQueue.peekFirst();
    }
    
    public String getNewline() {
        return newline;
    }

    public void setNewline(String newline) {
        this.newline = newline;
    }

    /**
     * Dumps all tokens. This is for development.
     * @return String representation of all TEXTokens generated.
     */
    public String dumpTokens() {
        StringBuilder builder = new StringBuilder();
        for (TEXToken tok: tokenQueue) {
            builder.append(tok).append(newline);
        }
        return builder.toString();
    }

}
